import React, { Fragment } from "react";
import { validationTypes } from "../../config";

const PasswordInput = ({ name, handler, touched, hasError, meta }) => {
    return (
        <Fragment>
            <div className="row">
                <div className="input-field col s12">
                    <input id={name} type="password" {...handler()} />
                    <label htmlFor={name}>{meta.label.toUpperCase()}</label>
                </div>
            </div>
            <div>
                <span className="warning" id="PasswordWarningNoValidPassword">
                    {touched
                        && hasError(validationTypes.REQUIRED)
                        && `${meta.label} is required`
                        || hasError(validationTypes.PATTERN)
                        &&
                        <span>
                            <label className="warning">{meta.label} is not a valid password your
                        password must at least:</label>
                            <ul>
                                <li>contain 1 capital letter</li>
                                <li>contain 1 lower case letter</li>
                                <li>contain 1 number</li>
                                <li>contain 1 special character</li>
                                <li>be 8 characters long</li>
                            </ul>
                        </span>
                        || hasError(validationTypes.EQUALITY)
                        && `Passwords do not match`
                    }
                </span>
            </div>
        </Fragment>
    );
}

export default PasswordInput;